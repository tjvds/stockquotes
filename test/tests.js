/**
 * Created by thomasjansen on 01/03/15.
 */
/*global QUnit, assert, stockquotes, conn, Gdata, window, module*/

QUnit.test("hello test", function (assert) {
    "use strict";
    assert.ok(1 === 1, "Passed!");
});

QUnit.test("Check if stockquotes is defined", function (assert) {
    "use strict";
    assert.ok(window.stockquotes !== undefined);
});

QUnit.test("Check if window is window", function (assert) {
    "use strict";
    assert.ok(window !== undefined);
});

QUnit.test("Check if Gdata is undefined", function (assert) {
    "use strict";
    assert.ok(Gdata === undefined);
});

QUnit.test("Check if module is defined", function (assert) {
    "use strict";
    assert.ok(module !== undefined);
});

QUnit.test("Check if init builds page", function (assert) {
    "use strict";
    assert.ok(module.init === module.buildPage);
});

