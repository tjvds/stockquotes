/*jslint browser : true , plusplus : true*/
/*global io, console, stockquotes, require*/

var expressServer = require('express'),
    app = expressServer(),
    server = require('http').createServer(app),
    io = require('socket.io').listen(server),
    XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest,
    lastQuoteJson;


(function () {
    "use strict";
    var module = {
        nodeRequest : function () {
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function () {
                if (this.readyState === 4) {
                    lastQuoteJson = JSON.parse(xhr.responseText);
                }
            };
            xhr.open("GET", "http://query.yahooapis.com/v1/public/yql?q=select%20Symbol%2C%20LastTradePriceOnly%2C%20LastTradeDate%2C%20LastTradeTime%2C%20Change%2C%20Open%2C%20DaysHigh%2C%20DaysLow%2C%20Volume%20%20%20from%20yahoo.finance.quotes%20where%20symbol%20in%0A(%22BCS%22%2C%22STT%22%2C%22JPM%22%2C%22LGEN.L%22%2C%22UBS%22%2C%22DB%22%2C%22BEN%22%2C%22CS%22%2C%22BK%22%2C%22KN.PA%22%2C%22GS%22%2C%22LM%22%2C%22MS%22%2C%22MTU%22%2C%22NTRS%22%2C%22GLE.PA%22%2C%22BAC%22%2C%22AV%22%2C%22SDR.L%22%2C%22DODGX%22%2C%22SLF%22%2C%22SL.L%22%2C%22NMR%22%2C%22ING%22%2C%22BNP.PA%22)&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=");
            xhr.send();
        },

        requestLoop : function () {
            module.nodeRequest();
            setInterval(module.nodeRequest, 4000);
        }
    };
    module.requestLoop();
}());


io.sockets.on('connection', function (socket) {
    "use strict";
    socket.emit('message', lastQuoteJson.query);
    var interval = setInterval(function () {
        socket.emit('message', lastQuoteJson.query);
    }, 4000);
    socket.on('disconnect', function () {
        clearInterval(interval);
    });
});

app.use("/javascript", expressServer.static(__dirname + '/javascript'));
app.use("/stylesheets", expressServer.static(__dirname + '/stylesheets'));

app.get("/", function (req, res) {
    "use strict";
    return res.sendfile(__dirname + "/index.html");
});

server.listen(3000);