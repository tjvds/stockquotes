/*jslint browser : true , plusplus : true*/
/*global io, console, stockquotes*/

var conn = io.connect('http://localhost:3000'), Gdata;

(function () {
    "use strict";
    var module = {
        buildPage : function () {
            var bodyNode, divNode, tableNode, rowNode, cellNode, i;

            bodyNode = document.getElementsByTagName("body")[0];
            bodyNode.innerHTML = "";
            divNode = document.createElement("div");
            tableNode = document.createElement("table");
            tableNode.setAttribute("Class", "table");
            divNode.appendChild(tableNode);

            rowNode = document.createElement("tr");
            cellNode = document.createElement("td");
            cellNode.innerHTML = "Symbol";
            rowNode.appendChild(cellNode);
            cellNode = document.createElement("td");
            cellNode.innerHTML = "DaysLow";
            rowNode.appendChild(cellNode);
            cellNode = document.createElement("td");
            cellNode.innerHTML = "DaysHigh";
            rowNode.appendChild(cellNode);
            cellNode = document.createElement("td");
            cellNode.innerHTML = "Change";
            rowNode.appendChild(cellNode);
            cellNode = document.createElement("td");
            cellNode.innerHTML = "LastTradeDate";
            rowNode.appendChild(cellNode);
            cellNode = document.createElement("td");
            cellNode.innerHTML = "LastTradePriceOnly";
            rowNode.appendChild(cellNode);
            tableNode.appendChild(rowNode);

            for (i = 0; i < Gdata.count; i++) {
                rowNode = document.createElement("tr");
                cellNode = document.createElement("td");
                cellNode.innerHTML = Gdata.results.quote[i].Symbol;
                rowNode.appendChild(cellNode);
                cellNode = document.createElement("td");
                cellNode.innerHTML = Gdata.results.quote[i].DaysLow;
                rowNode.appendChild(cellNode);
                cellNode = document.createElement("td");
                cellNode.innerHTML = Gdata.results.quote[i].DaysHigh;
                rowNode.appendChild(cellNode);
                cellNode = document.createElement("td");
                cellNode.innerHTML = Gdata.results.quote[i].Change;
                rowNode.appendChild(cellNode);
                cellNode = document.createElement("td");
                cellNode.innerHTML = Gdata.results.quote[i].LastTradeDate;
                rowNode.appendChild(cellNode);
                cellNode = document.createElement("td");
                cellNode.innerHTML = Gdata.results.quote[i].LastTradePriceOnly;
                if (Gdata.results.quote[i].Change > 0) {
                    rowNode.setAttribute("class", "up");
                } else if (Gdata.results.quote[i].Change < 0) {
                    rowNode.setAttribute("class", "down");
                }
                rowNode.appendChild(cellNode);
                tableNode.appendChild(rowNode);
            }
            bodyNode.appendChild(divNode);

        },

        init: function () {
            return module.buildPage();
        }
    };
    window.stockquotes = module;
}());






