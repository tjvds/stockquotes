/*jslint browser : true , plusplus : true*/
/*global io, console, stockquotes, conn, Gdata*/

conn.on('message', function (data) {
    "use strict";
    Gdata = data;
    window.stockquotes.buildPage();
});