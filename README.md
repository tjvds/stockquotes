Stockquotes
===========

Thomas Jansen van der Sligte

498183


Design ()
-------
A server which gets all the latest stocknumbers via a ajax request

Server send data using a websocket to the client.


Install ()
---------

Clone

run npm install

start app.js

go to Http://localhost:3000



Test ()
------------

Start server

Open test/test.html in browser.